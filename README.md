Quake Log
=================

O QuakeLog é um projeto desenvolvido em NodeJS que tem como objetivo converter um log do jogo Quake 3 em informações como: total de mortes, jogadores e a quantidade de mortes por jogador.

Através de uma API Rest, será possível obter as informações do log em JSON, conforme o exemplo abaixo:

```
[
  game_1: {
    total_kills: 45,
    players: ["Dono da bola", "Isgalamido", "Zeh"],
    kills: {
      "Dono da bola": 5,
      "Isgalamido": 18,
      "Zeh": 20
    }
  }
]
```

### NodeJS

Será necessário ter instaldo em sua máquina o [NodeJS](https://nodejs.org/en/) (versão >= 6.11.4) e o NPM (versão >= 3.10.10).

Ao fazer o download do projeto, abra um terminal no mesmo diretório onde esteja o arquivo package.json e execute o seguinte comando:

    $ npm install

### Iniciando a aplicação

Abra um terminal no diretório raiz do projeto e execute o seguinte comando:

    $ npm start

Após esses procedimentos, o servidor estará sendo executado na porta 3000. Para acessar a API basta acessar a URL: http://localhost:3000/quakeLogs

### Testes

Abra um terminal no diretório raiz do projeto e execute o seguinte comando:

    $ npm test
