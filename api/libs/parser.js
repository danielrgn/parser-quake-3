'use strict';

const fs = require('fs');

class Parser {

  constructor() {

  }

  reader(fileName) {
    return new Promise((resolve, reject) => {
      fs.readFile(fileName, function (err, logData) {
        if (err) {
          reject(err);
          return;
        }
        resolve(logData.toString());
      });
    });
  }

  parse(logTextString){
    return new Promise((resolve, reject) => {
      if(logTextString != null && typeof(logTextString) === 'string'){
        let games = [];
        let lineSplit = logTextString.split('\n');
        let count = 1;

        lineSplit.map( line => {
          let arrayLine = line.trim().split(/\\/);
          let wordsBreakSpace  = arrayLine[0].split(' ');

          switch(wordsBreakSpace[1]){
            case 'InitGame:' : initGame(games, count); count++; break;
            case 'ClientUserinfoChanged:': listPlayers(games, arrayLine); break;
            case 'Kill:' : calculatesKills(games, arrayLine); break;
          }
        });

        resolve(games);
      }else{
        reject('O arquivo de log não foi reconhecido. Conteúdo inválido.');
        return;
      }
    });
  }
}

// Inicializa os dados de um jogo
function initGame(games, count){
  games.push(
    {
      ['game_' + count] : {
        total_kills: 0,
        players: [],
        kills: {}
      }
    }
  );
}

// Lista os jogadores sem duplicidade
function listPlayers(games, arrayLine){
  let gameName = 'game_' + games.length;
  let game = games[games.length - 1][gameName];
  let players = game.players;
  let player =  arrayLine[1].trim();

  if(players.indexOf(player) <= -1){
    players.push(player);
    game.kills[player] = 0;
  }
}

// Calcula o número de kills total e por jogador
function calculatesKills(games, arrayLine){
  let gameName = 'game_' + games.length;
  let game = games[games.length - 1][gameName];
  game.total_kills++;

  if(arrayLine[0].includes('<world>')){
    qtyKillByWorld(game, arrayLine);
  }else{
    qtyKillByPlayer(game, arrayLine);
  }
}

// Responsavel por calcular as mortes causadas pelo mundo (<world>)
function qtyKillByWorld(game, arrayLine){
  let player = killedBy(arrayLine);
  game.kills[player]--;
}

// Responsavel por verificar qual é o jogador que foi morto
function killedBy(arrayLine){
  let killed = arrayLine[0].substr(arrayLine[0].lastIndexOf('killed')).substr(7);
  killed = killed.substr(0, killed.indexOf('by')).trim();
  return killed;
}

// Responsavel por calcular as mortes pelos players
function qtyKillByPlayer(game, arrayLine){
  let killer = playerKiller(arrayLine);
  let killed = killedBy(arrayLine);

  if(killer != killed){
    game.kills[killer]++;
  }else{
    game.kills[killer]--;
  }
}

// Responsavel por verificar qual é o jogador que matou
function playerKiller(arrayLine){
  let killer = arrayLine[0].substr(arrayLine[0].lastIndexOf(':')).substr(2);
  killer = killer.substr(0, killer.indexOf('killed')).trim();
  return killer;
}

module.exports = Parser;
