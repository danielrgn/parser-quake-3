'use strict';

module.exports = function(app){
  let api = app.controllers;

  app.route('/quakeLogs').get(api.quakeLogController.getLog);
};
