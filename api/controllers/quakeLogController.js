'use strict';

let Parser = require('../libs/parser');

var app = {};

app.getLog = function(req, res){

  let parser = new Parser();

  parser.reader('./log/quake3.log').then((txtLog) => {
    parser.parse(txtLog).then((games) => {
      res.status(200).json(games);
    }, (err) => {
      console.log(err);
      res.status(500).send(err);
    });
  }, (err) => {
    console.log(err);
    res.status(500).send("Não foi possível abrir o arquivo de log.");
  });
};

module.exports = app;
