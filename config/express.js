'use strict';

const express = require('express');
const app = express();
const consign = require('consign');

consign({cwd: 'api'})
  .include('controllers')
  .then('routes')
  .into(app);

module.exports = app;
