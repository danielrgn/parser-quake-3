let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;

let Parser = require('../api/libs/parser');

chai.use(chaiHttp);

describe('#Parse -> reader()', function () {
  it('#verificando se o arquivo de log depois de aberto é uma string', function (done) {

    let parser = new Parser();

    parser.reader('./log/quake3.log').then(txtLog => {
      txtLog.should.be.a('string');
      done();
    }, error => {
      done(error);
    });
  });
});

describe('#Parse -> parse()', function () {
  it('#verificando se o log parseado é um array de objetos', function (done) {

    let parser = new Parser();

    parser.reader('./log/quake3.log').then(txtLog => {
      parser.parse(txtLog).then(games => {
        games.should.to.be.a('array');
        games[0].should.to.be.a('object');
        done();
      }, error => {
        done(error);
      });
    }, error => {
      done(error);
    });
  });

  it('#tentando parsear um log com o texto vazio', function (done) {

    let parser = new Parser();

    parser.reader('./log/quake3.log').then(txtLog => {
      parser.parse('').then(games => {
        games.should.to.be.a('array');
        expect(games).to.be.empty;
        done();
      }, error => {
        done(error);
      });
    }, error => {
      done(error);
    });
  });
});

describe('#QuakeLog -> /GET/quakeLogs', function () {
  it('#verificando se o retorno da consulta é válida para o game_1', function (done) {

    chai.request(server).get('/quakeLogs').end(function (error, res) {
      if(error){
        done(error);
      }

      should.exist(res.body);
      res.should.have.status(200);
      res.body.should.be.an('array');
      res.body[0].should.be.an('object');
      res.body[0].game_1.should.be.an('object');
      res.body[0].game_1.should.have.property('total_kills');
      res.body[0].game_1.should.have.property('players');
      res.body[0].game_1.should.have.property('kills');
      done();
    });
  });

  it('#verificando se o total de kills é válida para o game_21', function (done) {

    chai.request(server).get('/quakeLogs').end(function (error, res) {
      if(error){
        done(error);
      }

      should.exist(res.body);
      res.should.have.status(200);
      res.body.should.be.an('array');
      res.body[20].should.be.an('object');
      res.body[20].game_21.should.have.property('total_kills');
      assert.equal(res.body[20].game_21.total_kills, 131, 'Não são iguais');
      done();
    });
  });

  it('#verificando os players do game_10', function (done) {

    chai.request(server).get('/quakeLogs').end(function (error, res) {
      if(error){
        done(error);
      }

      should.exist(res.body);
      res.should.have.status(200);
      res.body.should.be.an('array');
      res.body[9].should.be.an('object');
      res.body[9].game_10.should.have.property('players');
      assert.deepEqual(res.body[9].game_10.players, ['Oootsimo','Dono da Bola','Zeh','Chessus','Mal','Assasinu Credi','Isgalamido']);
      done();
    });
  });

  it('#verificando os kills dos jogadores do game_5', function (done) {

    chai.request(server).get('/quakeLogs').end(function (error, res) {
      if(error){
        done(error);
      }

      should.exist(res.body);
      res.should.have.status(200);
      res.body.should.be.an('array');
      res.body[4].should.be.an('object');
      res.body[4].game_5.should.have.property('kills');
      assert.deepEqual(res.body[4].game_5.kills, {"Dono da Bola":0,"Isgalamido":2,"Zeh":1,"Assasinu Credi":-3});
      done();
    });
  });
});
