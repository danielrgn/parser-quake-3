'use strict';

var app = require('./config/express');

const port = process.env.PORT || '3000';
app.set('port', port);

app.listen(port, () => console.log(`API running on localhost:${port}`));

module.exports = app;
